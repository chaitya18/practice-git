<?php
# src/MobileMoxie/HqBundle/Controller/HomeController.php

namespace MobileMoxie\HqBundle\Controller;

use FOS\UserBundle\Model\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MobileMoxie\AsoReportingBundle\Entity\Client;
use MobileMoxie\AsoReportingBundle\Entity\UserClientAccess;
use MobileMoxie\AsoReportingBundle\Entity\SearchTerm;
use MobileMoxie\AsoReportingBundle\Entity\App;

use Doctrine\Common\Util\Debug;

class HomeController extends Controller
{

	private function getCount( $repo, $month )
	{
		$month_from = $month;
		$month_to = $month -1;
		$months_ago_from = new \DateTime( date( 'Y-m-01 00:00:00' ) );
		$months_ago_from->sub( new \DateInterval( 'P'.$month_from.'M' ) );

		$months_ago_to = new \DateTime( date( 'Y-m-01 23:59:59' ) );
		$months_ago_to->sub( new \DateInterval( 'P'.$month_to.'M' ) );

		$query = $repo->createQueryBuilder( 'a' );
		$query->select( 'COUNT(a)' );
		$query->where( 'a.createdAt BETWEEN :from AND :to' );
		$query->setParameter( 'from', $months_ago_from->format('Y-m-d 00:00:00'));
		$query->setParameter( 'to', $months_ago_to->format('Y-m-d 23:59:59'));
		$count = $query->getQuery()->getSingleScalarResult();

		return array(
			'month' => $months_ago_from->format('M'),
			'value' => $count
		);
	}

	private function getCountCurrentMonth( $repo )
	{
		// stats for graphs
		$now = new \DateTime( date( 'Y-m-d 00:00:00' ) );
		$query = $repo->createQueryBuilder( 'a' );
		$query->select( 'COUNT(a)' );
		$query->where( 'a.createdAt BETWEEN :from AND :to' );
		$query->setParameter( 'from', $now->format('Y-m-01 00:00:00'));
		$query->setParameter( 'to', $now->format('Y-m-d 23:59:59'));
		$count = $query->getQuery()->getSingleScalarResult();

		return array(
			'month' => $now->format('M'),
			'value' => $count
		);
	}

	private function getCountForPaid( $repo, $month )
	{	
		if($month == 0){
			$now = new \DateTime( date( 'Y-m-d 00:00:00' ) );
			$months_ago_from = new \DateTime($now->format('Y-m-01 00:00:00'));
			$months_ago_to = new \DateTime($now->format('Y-m-d 23:59:59'));
		} else {

			$month_from = $month;
			$month_to = $month -1;
			$months_ago_from = new \DateTime( date( 'Y-m-01 00:00:00' ) );
			$months_ago_from->sub( new \DateInterval( 'P'.$month_from.'M' ) );

			$months_ago_to = new \DateTime( date( 'Y-m-01 23:59:59' ) );
			$months_ago_to->sub( new \DateInterval( 'P'.$month_to.'M' ) );
		}

		$query = $repo->createQueryBuilder( 'a' );
		$query->groupBy('a.client');
		$query->leftJoin('a.client', 'c');
		$query->andWhere('c.isTestClient = :isTest')
			  ->setParameter('isTest', 0);
		$arrPaid = $query->getQuery()->getResult();

		$count = 0;

		foreach($arrPaid as $paid){
			if($paid->getPaidAt() >=  $months_ago_from && $paid->getPaidAt() <=  $months_ago_to){
				$count++ ;
			}
		}

		return array(
			'month' => $months_ago_from->format('M'),
			'value' => $count
		);
	}

	private function getCountForFree( $repo, $month )
	{
		if($month == 0){
			$now = new \DateTime( date( 'Y-m-d 00:00:00' ) );
			$months_ago_from = new \DateTime($now->format('Y-m-01 00:00:00'));
			$months_ago_to = new \DateTime($now->format('Y-m-d 23:59:59'));
		} else {

			$month_from = $month;
			$month_to = $month -1;
			$months_ago_from = new \DateTime( date( 'Y-m-01 00:00:00' ) );
			$months_ago_from->sub( new \DateInterval( 'P'.$month_from.'M' ) );

			$months_ago_to = new \DateTime( date( 'Y-m-01 23:59:59' ) );
			$months_ago_to->sub( new \DateInterval( 'P'.$month_to.'M' ) );
		}

		$query = $repo->createQueryBuilder( 'a' );
		$query->select( 'COUNT(a)' );
		$query->where( 'a.createdAt BETWEEN :from AND :to' );
		$query->setParameter( 'from', $months_ago_from->format('Y-m-d 00:00:00'));
		$query->setParameter( 'to', $months_ago_to->format('Y-m-d 23:59:59'));
		$query->andWhere('a.subscriptionStatus != :status');
		$query->setParameter( 'status', 'Downgraded');
		$query->andWhere( 'a.isTestSubscription = :isTestSubscription' );
		$query->setParameter( 'isTestSubscription', '0');
		$count = $query->getQuery()->getSingleScalarResult();

		return array(
			'month' => $months_ago_from->format('M'),
			'value' => $count
		);
	}


	/**
	 * @Route("/hq/home/", name="hq_home")
	 */
	public function homeAction()
	{
        $user = $this->getUser();
        $isHqUser =  $user->hasRole( 'ROLE_HQ' );

        // is a non hq user has somehow made their way here, get them outta here
        if( !$isHqUser )
        {
            return $this->redirect( $this->generateUrl( 'hq_logout' ) );
        }

		$em               = $this->getDoctrine()->getManager();
		$client_repo      = $em->getRepository( 'MobileMoxieAsoReportingBundle:Client' );
		$member_repo      = $em->getRepository( 'ApplicationSonataUserBundle:User' );
		$app_repo         = $em->getRepository( 'MobileMoxieAsoReportingBundle:App' );
		$search_term_repo = $em->getRepository( 'MobileMoxieAsoReportingBundle:ClientSearchTerm' );
		$sub_repo         = $em->getRepository( 'AppBundle:Subscription' );
		$invoice_repo     = $em->getRepository( 'AppBundle:Invoice' );

		$bulk_address_result_screenshot_repo     = $em->getRepository( 'AppBundle:BulkAddressResultScreenshot' );

		$query = $client_repo->createQueryBuilder( 'a' );
		$query->select( 'COUNT(a)' );
		$query->where( 'a.isActive = :isActive' );
		$query->setParameter( 'isActive', 1 );
		$count_active_clients = $query->getQuery()->getSingleScalarResult();

		$query = $client_repo->createQueryBuilder( 'a' );
		$query->select( 'COUNT(a)' );
		$count_all_clients = $query->getQuery()->getSingleScalarResult();

		$query = $member_repo->createQueryBuilder( 'a' );
		$query->select( 'COUNT(a)' );
		$count_all_members = $query->getQuery()->getSingleScalarResult();

		$query = $app_repo->createQueryBuilder( 'a' );
		$query->select( 'COUNT(a)' );
		$count_all_apps = $query->getQuery()->getSingleScalarResult();

		$query = $search_term_repo->createQueryBuilder( 'a' );
		$query->select( 'COUNT(a)' );
		$count_all_search_terms = $query->getQuery()->getSingleScalarResult();



        $query = $bulk_address_result_screenshot_repo->createQueryBuilder( 'a' );
        $query->select( 'COUNT(a)' );
        $count_all_bulk_address_result_screenshot = $query->getQuery()->getSingleScalarResult();

		$now = new \DateTime( date( 'Y-m-d 00:00:00' ) );
		$months_ago_6 = new \DateTime( date( 'Y-m-01 00:00:00' ) );
		$months_ago_6->sub( new \DateInterval( "P6M" ) );

		// stats for client
		$clients_count[] = $this->getCount( $client_repo, 6 );
		$clients_count[] = $this->getCount( $client_repo, 5 );
		$clients_count[] = $this->getCount( $client_repo, 4 );
		$clients_count[] = $this->getCount( $client_repo, 3 );
		$clients_count[] = $this->getCount( $client_repo, 2 );
		$clients_count[] = $this->getCount( $client_repo, 1 );
		$clients_count[] = $this->getCountCurrentMonth( $client_repo);

		$clients_count_labels = array();
		$clients_count_data = array();
		foreach ( $clients_count as $count )
		{
			$clients_count_labels[] = $count['month'];
			$clients_count_data[] = $count['value'];
		}

		// stats for users
		$users_count[] = $this->getCount( $member_repo, 6 );
		$users_count[] = $this->getCount( $member_repo, 5 );
		$users_count[] = $this->getCount( $member_repo, 4 );
		$users_count[] = $this->getCount( $member_repo, 3 );
		$users_count[] = $this->getCount( $member_repo, 2 );
		$users_count[] = $this->getCount( $member_repo, 1 );
		$users_count[] = $this->getCountCurrentMonth( $member_repo);

		$users_count_labels = array();
		$users_count_data = array();
		$users_count_dates = array();
		$month_no = 7;
		foreach ( $users_count as $count )
		{
			$month_no--;
			$users_count_labels[] = $count['month'];
			$users_count_data[] = $count['value'];

			$months_ago_from = new \DateTime( date( 'Y-m-01 00:00:00' ) );
			$months_ago_from->sub( new \DateInterval( 'P'.$month_no.'M' ) );

			$months_ago_to = new \DateTime( date( 'Y-m-01 00:00:00' ) );
			if( $month_no > 0 )
			{
				$months_ago_to->sub( new \DateInterval( 'P'.($month_no-1).'M' ) );
			}
			else
			{
				$months_ago_to = new \DateTime($now->format('Y-m-d'));
			}


			$users_count_dates[] = array(
				'fromDate' => $months_ago_from->format('Y-m-d 00:00:00'),
				'toDate' => $months_ago_to->format('Y-m-d 23:59:59'),
				'month' => $count['month'],
			);
		}

		// stats for apps
		$apps_count[] = $this->getCount( $app_repo, 6 );
		$apps_count[] = $this->getCount( $app_repo, 5 );
		$apps_count[] = $this->getCount( $app_repo, 4 );
		$apps_count[] = $this->getCount( $app_repo, 3 );
		$apps_count[] = $this->getCount( $app_repo, 2 );
		$apps_count[] = $this->getCount( $app_repo, 1 );
		$apps_count[] = $this->getCountCurrentMonth( $app_repo);

		$apps_count_labels = array();
		$apps_count_data = array();
		foreach ( $apps_count as $count )
		{
			$apps_count_labels[] = $count['month'];
			$apps_count_data[] = $count['value'];
		}

		// stats for search_terms
		$search_terms_count[] = $this->getCount( $search_term_repo, 6 );
		$search_terms_count[] = $this->getCount( $search_term_repo, 5 );
		$search_terms_count[] = $this->getCount( $search_term_repo, 4 );
		$search_terms_count[] = $this->getCount( $search_term_repo, 3 );
		$search_terms_count[] = $this->getCount( $search_term_repo, 2 );
		$search_terms_count[] = $this->getCount( $search_term_repo, 1 );
		$search_terms_count[] = $this->getCountCurrentMonth( $search_term_repo);

		$search_terms_count_labels = array();
		$search_terms_count_data = array();
		foreach ( $search_terms_count as $count )
		{
			$search_terms_count_labels[] = $count['month'];
			$search_terms_count_data[] = $count['value'];
		}

		// stats for free trial count
		$sub_repo_count[] = $this->getCountForFree( $sub_repo, 6 );
		$sub_repo_count[] = $this->getCountForFree( $sub_repo, 5 );
		$sub_repo_count[] = $this->getCountForFree( $sub_repo, 4 );
		$sub_repo_count[] = $this->getCountForFree( $sub_repo, 3 );
		$sub_repo_count[] = $this->getCountForFree( $sub_repo, 2 );
		$sub_repo_count[] = $this->getCountForFree( $sub_repo, 1 );
		$sub_repo_count[] = $this->getCountForFree( $sub_repo, 0);
		
		$sub_repo_count_labels = array();
		$sub_repo_count_data = array();
		foreach ( $sub_repo_count as $count )
		{
			$sub_repo_count_labels[] = $count['month'];
			$sub_repo_count_data[] = $count['value'];
		}

		// stats for paid count
		$invoice_repo_count[] = $this->getCountForPaid( $invoice_repo, 6 );
		$invoice_repo_count[] = $this->getCountForPaid( $invoice_repo, 5 );
		$invoice_repo_count[] = $this->getCountForPaid( $invoice_repo, 4 );
		$invoice_repo_count[] = $this->getCountForPaid( $invoice_repo, 3 );
		$invoice_repo_count[] = $this->getCountForPaid( $invoice_repo, 2 );
		$invoice_repo_count[] = $this->getCountForPaid( $invoice_repo, 1 );
		$invoice_repo_count[] = $this->getCountForPaid( $invoice_repo, 0 );
		
		
		$invoice_repo_count_labels = array();
		$invoice_repo_count_data = array();
		$totalConverted = 0 ;
		
		$i = 0;
		
		foreach ( $invoice_repo_count as $count )
		{	
			
			if($sub_repo_count_data[$i] == 0){
				$cal = 0;	
			} else {
				$cal = ($count['value']/$sub_repo_count_data[$i])*100 ;
			}
			$invoice_repo_count_labels[] = $count['month'].'('.round($cal).'%)';

			$invoice_repo_count_data[] = $count['value'];
			$totalConverted = $totalConverted + $count['value'];
			$i ++;
		}


        // stats for bulk_address_result_screenshot
        // select c.businessName, count(scr.id) as total_serp_test_screenshots_in_march
        //from bulk_address_result_screenshot scr, `bulk_address_report_run` run, bulk_address_report report, client c
        //where scr.`bulkAddressReportRun_id`=run.id and run.`bulkAddressReport_id`=report.id and report.client_id = c.id
        //and run.createdAt > "2019-03-01 00:00:00"
        //group by c.businessName
        //order by total_serp_test_screenshots_in_march desc
        //
        //select c.businessName, count(scr.id) as total_bulk_page_test_screenshots_in_march
        //from bulk_page_result_screenshot scr, `bulk_page_report_run` run, bulk_page_report report, client c
        //where scr.`bulkPageReportRun_id`=run.id and run.`bulkPageReport_id`=report.id and report.client_id = c.id
        //and run.createdAt > "2019-03-01 00:00:00"
        //group by c.businessName
        //order by total_bulk_page_test_screenshots_in_march desc;


        $bulk_address_result_screenshot_count[] = $this->getCount( $bulk_address_result_screenshot_repo, 6 );
        $bulk_address_result_screenshot_count[] = $this->getCount( $bulk_address_result_screenshot_repo, 5 );
        $bulk_address_result_screenshot_count[] = $this->getCount( $bulk_address_result_screenshot_repo, 4 );
        $bulk_address_result_screenshot_count[] = $this->getCount( $bulk_address_result_screenshot_repo, 3 );
        $bulk_address_result_screenshot_count[] = $this->getCount( $bulk_address_result_screenshot_repo, 2 );
        $bulk_address_result_screenshot_count[] = $this->getCount( $bulk_address_result_screenshot_repo, 1 );
        $bulk_address_result_screenshot_count[] = $this->getCountCurrentMonth( $bulk_address_result_screenshot_repo);
  
        $bulk_address_result_screenshot_count_labels = array();
        $bulk_address_result_screenshot_count_data = array();
        foreach ( $bulk_address_result_screenshot_count as $count )
        {
            $bulk_address_result_screenshot_count_labels[] = $count['month'];
            $bulk_address_result_screenshot_count_data[] = $count['value'];
        }

		$data = array(
			'page_title'             => 'Home',
			'count_active_clients'   => number_format($count_active_clients),
			'count_all_clients'      => number_format($count_all_clients),
			'count_all_members'      => number_format($count_all_members),
			'count_all_apps'         => number_format($count_all_apps),
			'count_all_search_terms' => number_format($count_all_search_terms),

			'count_all_bulk_address_result_screenshot' => number_format($count_all_bulk_address_result_screenshot),

			'clients_count_labels' => array_values( $clients_count_labels ),
			'clients_count_data'   => array_values( $clients_count_data ),

			'users_count_labels' => array_values( $users_count_labels ),
			'users_count_data'   => array_values( $users_count_data ),
			'users_count_dates'   => $users_count_dates,

			'apps_count_labels' => array_values( $apps_count_labels ),
			'apps_count_data'   => array_values( $apps_count_data ),

			'search_terms_count_labels' => array_values( $search_terms_count_labels ),
			'search_terms_count_data'   => array_values( $search_terms_count_data ),

			'active_free_count_labels' => array_values( $sub_repo_count_labels ),
			'active_free_count_data'   => array_values( $sub_repo_count_data ),

			'paid_count_labels' => array_values( $invoice_repo_count_labels ),
			'paid_count_data'   => array_values( $invoice_repo_count_data ),
			'total_converted' => $totalConverted,

            'bulk_address_result_screenshot_count_labels' => array_values( $bulk_address_result_screenshot_count_labels ),
            'bulk_address_result_screenshot_count_data'   => array_values( $bulk_address_result_screenshot_count_data ),

			'from_date' => $months_ago_6->format( 'm/d/Y' ),
			'to_date'   => $now->format( 'm/d/Y' ),
		);

		return $this->render( ':Hq/Home:home.html.twig', $data );
	}
}
