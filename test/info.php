<?php 
session_start();
include 'config.php';
if(!isset($_SESSION['user'])){
 	header('location:login.html');
 	session_unset();
 	session_destroy();
 }
?>
<!doctype html>
<html lang="en">
  <head>
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 

    <title>Welcome @<?php echo $_SESSION['user'];?></title>
 </head>
  <body>
 <div class="container-lg">
   <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a href="#" class="navbar-brand text-warning">Welcome <?php echo $_SESSION['user'];?></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbaritems">
       <span class="navbar-toggler-icon"></span>
     </button>
     <div class="collapse navbar-collapse"id="navbaritems">
       <ul class="navbar-nav ml-auto">
         <li class="nav-item">
         </li>
          <li class="nav-item active">
           <a href="logout.php" class="nav-link text-uppercase btn btn-outline-info mr-4">Logout</a>
         </li>
        </ul>
     </div>
   </nav>
 </div>
 <div class="container mt-5">
 	<h2 class="text-capitalize text-center text-danger">User Profile</h2>
<?php
$username = $_SESSION['user'];
$query = "SELECT * from register where username = '$username'";
$result = $conn->query($query);
if(mysqli_num_rows($result)){
while($row = $result->fetch_assoc()){
	echo "<table class='table d-table d-flex text-center justify-content-center mt-4'>";
	echo "<tr>";
	echo "<th>User name</th>";
	echo "<th>hobby</th>";
	echo "<th>gender</th>";
	echo "<th>Country</th>";
	echo "<th>State</th>";
	echo "<th>City</th>";
	echo "</tr>";
	echo "<tr>";
	echo "<td>".$row['username']."</td>";
	echo "<td>".$row['hobby']."</td>";
	echo "<td>".$row['gender']."</td>";
	echo "<td>".$row['country']."</td>";
	echo "<td>".$row['state']."</td>";
	echo "<td>".$row['city']."</td>";
	echo "</tr>";
}
}


?>
</div>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>