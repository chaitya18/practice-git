$(document).ready(function(){
  showdata();
});

$('#search').keyup(function(){
  var user = $("#search").val();
  $.ajax({
  url:'search.php',
  type:'POST',
  data:{username:user},
  success:function(result){
    $("#tbody").html(result);
  }
});
});

function showdata(){
  $.ajax({
  url:'listalluser.php',
  type:'GET',
  success:function(result){
  $('#tbody').html(result);
  }
  });
}

function deleteuser(userid){
var conf = confirm('Are You Sure ?');
if(conf==true)
{
    $.ajax({
    url:'delete.php',
    type:'POST',
    data:{uid:userid},
    success:function(result)
    {
    showdata();
    }
    });
}
}

function updateuser(userid){
  $.ajax({
    url: 'update.php',
    type: 'POST',
    data: {uid:userid},
    success:function(result){
      SetValues(result);

    }
  });
 
}
id='';
function SetValues(data){
  $("input[type='checkbox']").prop("checked",false);
  var readData = JSON.parse(data);
  var id = readData[0].id;
  var username = readData[0].username;
  var hobby = readData[0].hobby.split(",");
  var gender = readData[0].gender;
  var country = readData[0].country;
  var state = readData[0].state;
  var city = readData[0].city;


  // console.log(id,username,hobby,gender,country,state,city);
$('#username').val(username);
/*setting up checkbox value*/        
for (var i = 0; i<hobby.length;i++){
  $("input[value="+hobby[i]+"]").prop("checked",true);
  
}
//Gender values
$("input[value="+gender+"]").attr("checked",true);
$("#country").html('<option>'+country+'</option>');
$("#country").val(country);
$("#city").html("<option>"+city+"</option>");
$("#state").html("<option>"+state+"</option>");
/*setting countries*/
$("#hidden").val(id);
$.ajax({
      url:'https://api.jsonbin.io/b/6024c96987173a3d2f5b81cd',
      type:'GET',
      datatype:'json',
      success:function(result){
        $.each(result,function(i,countryname){
          $('#country').append("<option value='"+countryname.code+"'>"
            +countryname.name+
          "</option>");
        });
      }
  
  });

  if($("#country").val()=="INDIA")
  {
    $.ajax({
        url:'https://api.jsonbin.io/b/6024ca2287173a3d2f5b822c',
        type:'GET',
        datatype:'json',
        success:function(result)
        {
          $.each(result,function(statecode,statename){
            $('#state').append("<option value='"+statename+"'>"
            +statename+"</option>");
          });
        }
      });
  }


  if($("#state").val()=="Goa"){
    $.ajax
        ({
          url:'https://api.jsonbin.io/b/60361c5bf1be644b0a6422e5',
          type:'GET',
          datatype:'json',
          success:function(result)
          {
            $.each(result,function(citycode,cityname)
            {
              $('#city').append("<option value='"+cityname+"'>"
                +cityname+"</option>");
            });
          }
          
        });
  }
   if($("#state").val()=="Gujarat"){
          $.ajax({
          url:'https://api.jsonbin.io/b/60361628f1be644b0a642094/2',
          type:'GET',
          datatype:'json',
          success:function(result)
          {
            $.each(result,function(citycode,cityname)
            {
              $('#city').append("<option value='"+cityname+"'>"
                +cityname+"</option>");
            });
          }
          
        });
  }
  if($("#state").val()=="Himachal Pradesh"){
          $.ajax({
          url:'https://api.jsonbin.io/b/60361dc10866664b10824dcb',
          type:'GET',
          datatype:'json',
          success:function(result)
          {
            $.each(result,function(citycode,cityname)
            {
              $('#city').append("<option value='"+cityname+"'>"
                +cityname+"</option>");
            });
          }
          
        });
  }


$(document).ready(function()
{
/*Get Counteries Using Ajax*/
  $.ajax({
      url:'https://api.jsonbin.io/b/6024c96987173a3d2f5b81cd',
      type:'GET',
      datatype:'json',
      success:function(result){
        $.each(result,function(i,countryname){
          $('#country').append("<option value='"+countryname.code+"'>"
            +countryname.name+
          "</option>");
        });
      }
  
  });
/*Get State using ajax*/
  $('#country').change(function()
   {
    if($(this).val()=='IN'|| $(this).val()=='INDIA')
    {
      $("#state").html("");

      $.ajax({
        url:'https://api.jsonbin.io/b/6024ca2287173a3d2f5b822c',
        type:'GET',
        datatype:'json',
        success:function(result)
        {
          $.each(result,function(statecode,statename){
            $('#state').append("<option value='"+statecode+"'>"
            +statename+"</option>");
          });
        }
      });
    }else
    {
      $("#state").html("<option>please select state</option>");
    }
  });
    

/*City using ajax*/
$('#state').change(function()
    {
      if($(this).val()=='GJ')
      {
        $("#city").html("");
        
        $.ajax
        ({
          url:'https://api.jsonbin.io/b/60361628f1be644b0a642094/2',
          type:'GET',
          datatype:'json',
          success:function(result)
          {
            $.each(result,function(citycode,cityname)
            {
              $('#city').append("<option value='"+cityname+"'>"
                +cityname+"</option>");
            });
          }
          
        });
      }else{
        $("#city").html("<option>Please Select City</option>");
      }
});


  $('#state').change(function(){
    if($(this).val()=='GA'){
      $('#city').html("");
      
      $.ajax({
        url:'https://api.jsonbin.io/b/60361c5bf1be644b0a6422e5',
        type:"GET",
        datatype:'json',
        success:function(result){
          $.each(result,function(citycode,cityname){
              $('#city').append("<option value='"+cityname+"'>"
                +cityname+"</option>");
          });
        }
      });
    }else{
        $("#city").html("<option>Please Select City</option>");
       }
  });
  

$('#state').change(function(){
    if($(this).val()=='HP'){
      $('#city').html("");
      $.ajax({
        url:'https://api.jsonbin.io/b/60361dc10866664b10824dcb',
        type:"GET",
        datatype:'json',
        success:function(result){
          $.each(result,function(citycode,cityname){
              $('#city').append("<option value='"+cityname+"'>"
                +cityname+"</option>");
          });
        }
      });
    }else
    {
    $("#city").html("<option>Please Select City</option>");
    }
  });
});
}

$(document).ready(function(){
  $("#submit").click(function(){
    submitdata();
    function submitdata(){
    var new_username = $("#username").val();
    var hobby = [];
  $.each($('input[name = "hobby"]:checked'),function(){
      hobby.push($(this).val());
  });
  var data = hobby.join(", ");
  var gender = [];
  $.each($('input[name = "gender"]:checked'),function(){
    gender.push($(this).val());
  });
  var data2 = gender.join(", ");

  if($("#country").val()=='IN'||$("#country").val()=='INDIA'){
    var country = "India";
  }
  if($("#state").val()=="GJ"||$("#state").val()=="Gujarat"){
    var state = "Gujarat";
  }else if($("#state").val()=="GA"||$("#state").val()=="Goa"){
    var state = "Goa";
  }else if($("#state").val()=="HP"||$("#state").val()=="Himachal Pradesh"){
    var state = "Himachal Pradesh";
  }
  var city = $("#city").val();
  var id = $("#hidden").val();
  // console.log(id,new_username, data, data2,country,state,city)
  $.ajax({
    url: 'updatedata.php',
    type: 'post',
    data: {
      id:id,
      new_username:new_username,
      new_hobby:data,
      new_gender:data2,
      new_country:country,
      new_state:state,
      new_city:city,
    },
    success:function(result){
      console.log(result);
      showdata();
    }
  });

  
}
  });
});
