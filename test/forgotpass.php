
<!doctype html>
<html lang="en">
  <head>
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 
<script type="text/javascript"src="validation.js"></script>
    <title>Reset Password Form</title>
 </head>
  <body>
 <div class="container-lg">
   <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a href="#" class="navbar-brand text-warning">Regisertation form</a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbaritems">
       <span class="navbar-toggler-icon"></span>
     </button>
     <div class="collapse navbar-collapse"id="navbaritems">
       <ul class="navbar-nav ml-auto">
         <li class="nav-item">
           <a href="index.html" class="nav-link text-uppercase btn btn-outline-primary mr-4">Register Data</a>
         </li>
          <li class="nav-item active">
           <a href="login.html" class="nav-link text-uppercase btn btn-outline-info mr-4">Login</a>
         </li>
        </ul>
     </div>
   </nav>
 </div>
   <h2 class="text-center text-info text-capitalize">Reset Password</h2>

<div class="container-fluid d-flex justify-content-center">
    <div class="col-md-6 ">
      <form method="post" action="forgot.php">
        <div class="form-group border border-info" style="border-radius: 30px;">
        <div class="col-12 pl-4 pr-4 pt-3 pb-2">
          <label for="username">Username :</label>
          <input type="text" class="form-control" name="username" id="username">
          <span id="username_error"></span>
        </div>
        <div class="col-md-12 mt-4 d-flex justify-content-center pb-5">
          <input type="submit" name="submit" class="btn btn-outline-warning">
        </div>
       
        </div>
      </form>
    </div>
  </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>