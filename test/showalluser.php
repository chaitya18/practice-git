
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="update.js"></script>
    <title>All User Details</title>
  </head>
  <body>
    <div class="container-lg">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a href="#" class="navbar-brand text-warning">User Detail</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbaritems">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse"id="navbaritems">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            </li>
          </ul>
        </div>
      </nav>
    </div>
    <h2 class="text-center text-info text-capitalize">All Users</h2>
    <div class="container">
      <form class="form-inline d-flex justify-content-end">
        <input class="form-control m-sm-3" type="search" id="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
      </form>
      <table class="table table-responsive">
        <thead>
          <tr>
            <th>ID</th>
            <th>User name</th>
            <th>Hobby</th>
            <th>Gender</th>
            <th>Country</th>
            <th>State</th>
            <th>State</th>
          </tr>
        </thead>
        <tbody id="tbody">
          
        </tbody>
      </table>
      
    </div>
    <!-- Button trigger modal -->
    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Update User Detail</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6">
                    <label for="username" class="form-text">User Name: </label>
                    <input type="email" name="username" id="username" class="form-control">
                    <span id="username_error"></span>
                  </div>
                  <div class="col-md-9 mt-4">
                    <label for="hobbies">Select Hobby</label>
                    <div class="form-check">
                      <input type="checkbox" name="hobby" value="sports" class="form-check-input">
                      <label class="form-check-label" for="hobby">Sports</label>
                    </div>
                    <div class="form-check">
                      <input type="checkbox" name="hobby" value="reading" class="form-check-input">
                      <label class="form-check-label" for="hobby">Reading</label>
                    </div>
                    <div class="form-check">
                      <input type="checkbox" name="hobby" value="driving" class="form-check-input">
                      <label class="form-check-label" for="hobby">Driving</label>
                    </div>
                    
                    <div class="form-check">
                      <input type="checkbox" name="hobby" value="games" class="form-check-input">
                      <label class="form-check-label" for="hobby">Games</label>
                    </div>
                  </div>
                  <div class="col-md-12 mt-3">
                    <label>Select Gender</label>
                    <div class="col-md-3">
                      <input type="radio" name="gender" class="form-check-input" value="Male">Male
                    </div>
                    <div class="col-md-8">
                      <input type="radio" name="gender" class="form-check-input" value="Female">Female
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="row">
                      <div class="col-md-12 mt-3">
                        <label>Select Country</label>
                        <select class="form-control" id="country" name="country" required>
                          <option value="">Select Country</option>
                        </select>
                      </div>
                      <div class="col-md-12 mt-3">
                        <label>Select state</label>
                        <select class="form-control" id="state" name="state" required>
                          <option value="">Select State</option>
                        </select>
                      </div>
                      <div class="col-md-12 mt-3">
                        <label>Select City</label>
                        <select class="form-control" id="city" name="city" required>
                          <option value="">Select City</option>
                        </select>
                      </div>                      
                    </div>
                  </div>
                  
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <input type="submit" name="submit" id="submit" data-dismiss="modal"  class="btn btn-success form-control">
          <input type="hidden" name="hidden" id="hidden">
          </div>
        </div>
      </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 <script type="text/javascript">
   $('#search').keyup(function(){
  var user = $("#search").val();
  $.ajax({
  url:'search.php',
  type:'POST',
  data:{username:user},
  success:function(result){
    $("#tbody").html(result);
  }
});
});
 </script>
  </body>
</html>