$(document).ready(function()
{
/*Get Counteries Using Ajax*/
	$.ajax({
			url:'https://api.jsonbin.io/b/6024c96987173a3d2f5b81cd',
			type:'GET',
			datatype:'json',
			success:function(result){
				$.each(result,function(i,countryname){
					$('#country').append("<option value='"+countryname.code+"'>"
						+countryname.name+
					"</option>");
				});
			}
	
	});
/*Get State using ajax*/
	$('#country').change(function()
	 {
		if($(this).val()=='IN')
		{
			$("#state").html("");

			$.ajax({
				url:'https://api.jsonbin.io/b/6024ca2287173a3d2f5b822c',
				type:'GET',
				datatype:'json',
				success:function(result)
				{
					$.each(result,function(statecode,statename){
						$('#state').append("<option value='"+statecode+"'>"
						+statename+"</option>");
					});
				}
			});
		}else
		{
			$("#state").html("<option>please select state</option>");
		}
	});
		

/*City using ajax*/
$('#state').change(function()
		{
			if($(this).val()=='GJ')
			{
				$("#city").html("");
				
				$.ajax
				({
					url:'https://api.jsonbin.io/b/60361628f1be644b0a642094/2',
					type:'GET',
					datatype:'json',
					success:function(result)
					{
						$.each(result,function(citycode,cityname)
						{
							$('#city').append("<option value='"+cityname+"'>"
								+cityname+"</option>");
						});
					}
					
				});
			}else{
				$("#city").html("<option>Please Select City</option>");
			}
});


	$('#state').change(function(){
		if($(this).val()=='GA'){
			$('#city').html("");
			
			$.ajax({
				url:'https://api.jsonbin.io/b/60361c5bf1be644b0a6422e5',
				type:"GET",
				datatype:'json',
				success:function(result){
					$.each(result,function(citycode,cityname){
							$('#city').append("<option value='"+cityname+"'>"
								+cityname+"</option>");
					});
				}
			});
		}else{
				$("#city").html("<option>Please Select City</option>");
			 }
	});
	

$('#state').change(function(){
		if($(this).val()=='HP'){
			$('#city').html("");
			$.ajax({
				url:'https://api.jsonbin.io/b/60361dc10866664b10824dcb',
				type:"GET",
				datatype:'json',
				success:function(result){
					$.each(result,function(citycode,cityname){
							$('#city').append("<option value='"+cityname+"'>"
								+cityname+"</option>");
					});
				}
			});
		}else
		{
		$("#city").html("<option>Please Select City</option>");
		}
	});

	$('#username').keyup(function() {
		validate_username();
	});
	$('#password').keyup(function(){
		validate_password();
	});
	$('#cpassword').keyup(function(){
		validate_cpassword();
	});

	$('input[type=checkbox]').click(function(){
		var hobby = [];
		$.each($('input[name = "hobby"]:checked'),function(){
			hobby.push($(this).val());
		
		});
		var data = "You have selected ",hobby;
		$("#checkeditems").html(hobby.join(", "));
		validate_hobby(hobby);
	});
	

	$('input[type=radio]').click(function(){
		var gender = [];
		$.each($('input[name = "gender"]:checked'),function(){
			gender.push($(this).val());
		});
		$("#selecteditems").html("your Gender is : "+gender);

	});

$('#username_error').hide();
	$('#password_error').hide();
	$('#cpassword_error').hide();

	var error_username = true;
	var error_password = true;
	var error_cpassword = true;
	var error_hobby = true;
function validate_username()
{
var username = $('#username').val();
if(username.length ==""){
		$('#username_error').show();
		$('#username_error').html('**please enter your first name here');
		$('#username_error').css('color', 'red');
		error_username = false;
		return false;
}

var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				  if(regex.test(username)!=true){
				  $('#username_error').show();
					$('#username_error').html("**email id should be like this ex:abc@gmail.com");
					$('#username_error').focus();
					$('#username_error').css('color', 'red');
					error_username = false;
					return false;
					
				  }else{
				  	$("#username_error").hide();
				  }
}

function validate_password(){
	var pass = $('#password').val();
	if(pass.length==''){
		$('#password_error').show();
		$('#password_error').html('**please enter your password here');
		$('#password_error').css('color', 'red');
		error_password = false;
		return false;
	}
	else if(pass.length<6 || pass.length>12){
		$('#password_error').show();
		$('#password_error').html('**your password must be between 6 to 12');
		$('#password_error').css('color', 'red');
		error_password = false;
		return false;
	}else{
		$("#password_error").hide();
		
	}

}

function validate_cpassword()
{
	var rpass = $('#password').val();
	var cpass = $('#cpassword').val();
	if(cpass.length=="")
	{
		$('#cpassword_error').show();
		$('#cpassword_error').html("**Please Enter your Confirm Password here!");
		$('#cpassword_error').css('color','red');
		error_cpassword = false;
		return false;
	}
	else if(cpass !== rpass){
		$('#cpassword_error').show();
		$('#cpassword_error').html("**Your password is not matching");
		$('#cpassword_error').css('color','red');
		error_cpassword = false;
		return false;
	}else{
		$("#cpassword_error").hide();
	
	}

}
function validate_hobby(hobby)
{
	$("#submit").click(function(){
		if(hobby.length<2){
			alert("minimum 2 required");
			error_hobby = false;
		}
	});
}

$("#submit").click(function(e){

	e.preventDefault();

	error_username = true;
	error_password = true;
	error_cpassword = true;
	error_hobby = true;
	validate_cpassword();
	validate_password();
	validate_username();
	validate_hobby();
	if(error_username==true && error_password==true && error_cpassword==true && error_hobby==true){

	var username = $('#username').val();
	var password = $("#password").val();
	var cpassword = $("#cpassword").val();
	var hobby = [];
	$.each($('input[name = "hobby"]:checked'),function(){
			hobby.push($(this).val());
	});
	
	var data = hobby.join(", ");
	var gender = [];
	$.each($('input[name = "gender"]:checked'),function(){
		gender.push($(this).val());
	});
	var data2 = gender.join(", ");

	if($("#country").val()=='IN'){
		var country = "INDIA";
	}
	if($("#state").val()=="GJ"){
		var state = "Gujarat";
	}else if($("#state").val()=="GA"){
		var state = "Goa";
	}else if($("#state").val()=="HP"){
		var state = "Himachal Pradesh";
	}
	var city = $("#city").val();

	$.ajax({
		url : 'register.php',
		type:'post',
		data:{
			username:username,
			password:password,
			cpassword:cpassword,
			hobby:data,
			gender:data2,
			country:country,
			state:state,
			city:city
		},
		success:function(data){
			console.log(data);
			$("#form1")[0].reset();
			$("#city").html("");
			$("#state").html("");

		},
		error:function(data){
			console.log(data);

		},
		complete:function(){
			console.log("completed");
		}
	});

	}
	else{
		return false;
	}
});
});

	